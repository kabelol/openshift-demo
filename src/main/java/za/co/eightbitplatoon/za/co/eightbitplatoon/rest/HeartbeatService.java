package za.co.eightbitplatoon.za.co.eightbitplatoon.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.time.LocalTime;

@Path("/heartbeat")
public class HeartbeatService {
    @GET
    @Produces("text/html")
    public String heartbeat() {
        return LocalTime.now().toString();
    }

}
